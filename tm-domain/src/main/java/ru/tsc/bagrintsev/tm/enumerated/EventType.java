package ru.tsc.bagrintsev.tm.enumerated;

public enum EventType {

    POST_INSERT,
    POST_DELETE,
    POST_UPDATE,

}
