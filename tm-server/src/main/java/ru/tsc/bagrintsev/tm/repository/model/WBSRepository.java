package ru.tsc.bagrintsev.tm.repository.model;

import jakarta.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.model.IWBSRepository;
import ru.tsc.bagrintsev.tm.model.AbstractWBSModel;

import java.util.List;

public class WBSRepository<M extends AbstractWBSModel> extends UserOwnedRepository<M> implements IWBSRepository<M> {

    public WBSRepository(
            @NotNull final Class<M> clazz,
            @NotNull final EntityManager entityManager
    ) {
        super(clazz, entityManager);
    }

    @Override
    public @Nullable List<M> findAllSort(
            @NotNull final String userId,
            @NotNull final String order
    ) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.user.id = :userId ORDER BY %s",
                clazz.getSimpleName(),
                order);
        return entityManager
                .createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        @NotNull final String jpql = String.format("" +
                        "UPDATE %s m " +
                        "SET m.name = :name, " +
                        "m.description = :description " +
                        "WHERE m.user.id = :userId " +
                        "AND m.id = :id",
                clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setParameter("name", name)
                .setParameter("description", description)
                .executeUpdate();
    }

}
