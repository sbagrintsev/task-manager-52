package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.sevice.dto.*;

public interface IServiceLocator {

    @NotNull
    IAuthServiceDTO getAuthService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectServiceDTO getProjectService();

    @NotNull
    IProjectTaskServiceDTO getProjectTaskService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITaskServiceDTO getTaskService();

    @NotNull
    IUserServiceDTO getUserService();

}
