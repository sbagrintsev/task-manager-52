package ru.tsc.bagrintsev.tm.api.sevice;

import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;

import javax.jms.JMSException;

public interface ISenderService {

    void createMessage(
            @NotNull Object object,
            @NotNull String eventType
    );

    void initJMS(@NotNull BrokerService broker) throws Exception;

    void stop() throws JMSException;

    void stopJMS(@NotNull BrokerService broker) throws Exception;

}
