package ru.tsc.sbagrintsev.tm.service.model;

import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

@Category(DBCategory.class)
public final class AuthServiceTest extends AbstractTest {

    @Nullable
    private String token;

    @Before
    public void setUp() {
        token = null;
    }

    @Test
    @Category(DBCategory.class)
    public void testSignIn() throws Exception {
        Assert.assertNull(token);
        token = authService.signIn("test1", "testPassword1");
        Assert.assertNotNull(token);
    }

    @Test
    @Category(DBCategory.class)
    public void testValidateToken() throws Exception {
        token = authService.signIn("test1", "testPassword1");
        Assert.assertNotNull(token);
        Assert.assertNotNull(authService.validateToken(token));
    }

}